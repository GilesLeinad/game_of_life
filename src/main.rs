use game_of_life::{Cell, Game};

mod game_of_life;
mod gui;

fn main() {
    let mut game = Game::new(50, 30);

    // Blinker
    game.set_cell_state(5, 20, Cell::Alive);
    game.set_cell_state(5, 21, Cell::Alive);
    game.set_cell_state(5, 22, Cell::Alive);

    // Glider
    game.set_cell_state(1, 0, Cell::Alive);
    game.set_cell_state(2, 1, Cell::Alive);
    game.set_cell_state(0, 2, Cell::Alive);
    game.set_cell_state(1, 2, Cell::Alive);
    game.set_cell_state(2, 2, Cell::Alive);

    gui::run(game);
}
