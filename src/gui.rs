use crate::game_of_life::{Cell, Game};

use speedy2d::color::Color;
use speedy2d::dimen::Vector2;
use speedy2d::shape::Rectangle;
use speedy2d::window::{WindowHandler, WindowHelper};
use speedy2d::{Graphics2D, Window};

use std::{thread, time};

pub fn run(game: Game) {
    let window = Window::new_centered("Game Of Life", (640, 480)).unwrap();
    let user_event_sender = window.create_user_event_sender();

    thread::spawn(move || loop {
        thread::sleep(time::Duration::from_millis(250));
        let _res = user_event_sender.send_event(());
    });

    window.run_loop(MyWindowHandler { game });
}

struct MyWindowHandler {
    game: Game,
}

impl WindowHandler for MyWindowHandler {
    fn on_draw(&mut self, _helper: &mut WindowHelper, graphics: &mut Graphics2D) {
        let tile_size = 10.0;
        let tile_spacing = 1.0;
        let alive_color = Color::GREEN;
        let dead_color = Color::GRAY;
        graphics.clear_screen(Color::from_rgb(0.8, 0.9, 1.0));

        for y_usize in 0..self.game.get_y_size() {
            let y = y_usize as f32;

            for x_usize in 0..self.game.get_x_size() {
                let x = x_usize as f32;

                let mut color = dead_color;

                if self.game.get_cell_state(x_usize, y_usize) == Cell::Alive {
                    color = alive_color;
                }

                graphics.draw_rectangle(
                    Rectangle::new(
                        Vector2 {
                            x: x * tile_size + x * tile_spacing,
                            y: y * tile_size + y * tile_spacing,
                        },
                        Vector2 {
                            x: x * tile_size + x * tile_spacing + tile_size,
                            y: y * tile_size + y * tile_spacing + tile_size,
                        },
                    ),
                    color,
                )
            }
        }
    }

    fn on_user_event(&mut self, helper: &mut WindowHelper<()>, _user_event: ()) {
        self.game.determine_next_state();
        helper.request_redraw()
    }
}
