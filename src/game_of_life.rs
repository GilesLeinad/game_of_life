#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Cell {
    Alive,
    Dead,
}

#[derive(Debug)]
pub struct Game {
    x_size: usize,
    y_size: usize,
    cells: Vec<Vec<Cell>>,
}

impl Game {
    /**
     * Creates a new game with the given size where all cells a are initialized as [`Cell::Dead`].
     */
    pub fn new(x_size: usize, y_size: usize) -> Self {
        let cells = vec![vec![Cell::Dead; x_size]; y_size];
        Game {
            x_size,
            y_size,
            cells,
        }
    }

    pub fn determine_next_state(&mut self) {
        let mut next = self.cells.clone();

        for (y, row) in self.cells.iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                let number_of_living_neighbors = self.get_number_of_living_neighbors(x, y);

                let new_cell_state = match (cell, number_of_living_neighbors) {
                    (Cell::Alive, 0..=1) => Cell::Dead,
                    (Cell::Alive, 2..=3) => Cell::Alive,
                    (Cell::Alive, 4..=usize::MAX) => Cell::Dead,
                    (Cell::Dead, 3) => Cell::Alive,
                    (current_state, _) => *current_state,
                };

                next[y][x] = new_cell_state;
            }
        }

        self.cells = next;
    }

    pub fn get_x_size(&self) -> usize {
        self.x_size
    }

    pub fn get_y_size(&self) -> usize {
        self.y_size
    }

    pub fn set_cell_state(&mut self, x: usize, y: usize, state: Cell) {
        let cell = self.cells.get_mut(y).unwrap().get_mut(x).unwrap();
        *cell = state;
    }

    pub fn get_cell_state(&self, x: usize, y: usize) -> Cell {
        self.cells[y][x]
    }

    fn get_number_of_living_neighbors(&self, x: usize, y: usize) -> usize {
        let mut number_of_living_neighbors: usize = 0;

        let offsets: Vec<(i32, i32)> = vec![
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ];

        for offset in offsets {
            let x_neighbor = x as i32 + offset.0;
            let y_neighbor = y as i32 + offset.1;

            if (0..self.x_size as i32).contains(&x_neighbor)
                && (0..self.y_size as i32).contains(&y_neighbor)
                && self.cells[y_neighbor as usize][x_neighbor as usize] == Cell::Alive
            {
                number_of_living_neighbors += 1;
            }
        }

        number_of_living_neighbors
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn a_living_cell_with_no_neighbors_dies() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(1, 1, Cell::Alive);
        game.determine_next_state();
        assert_eq!(game.get_cell_state(1, 1), Cell::Dead);
    }

    #[test]
    fn a_living_cell_with_one_neighbor_dies() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(1, 1, Cell::Alive);
        game.set_cell_state(0, 0, Cell::Alive);
        game.determine_next_state();
        assert_eq!(game.get_cell_state(1, 1), Cell::Dead);
    }

    #[test]
    fn a_living_cell_with_two_neighbors_stays_alive() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(1, 1, Cell::Alive);
        game.set_cell_state(0, 0, Cell::Alive);
        game.set_cell_state(2, 2, Cell::Alive);
        game.determine_next_state();
        assert_eq!(game.get_cell_state(1, 1), Cell::Alive);
    }

    #[test]
    fn a_living_cell_with_three_neighbors_stays_alive() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(1, 1, Cell::Alive);
        game.set_cell_state(0, 0, Cell::Alive);
        game.set_cell_state(2, 2, Cell::Alive);
        game.set_cell_state(2, 0, Cell::Alive);
        game.determine_next_state();
        assert_eq!(game.get_cell_state(1, 1), Cell::Alive);
    }

    #[test]
    fn a_living_cell_with_four_neighbors_dies() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(1, 1, Cell::Alive);
        game.set_cell_state(0, 0, Cell::Alive);
        game.set_cell_state(2, 2, Cell::Alive);
        game.set_cell_state(2, 0, Cell::Alive);
        game.set_cell_state(0, 2, Cell::Alive);
        game.determine_next_state();
        assert_eq!(game.get_cell_state(1, 1), Cell::Dead);
    }

    #[test]
    fn a_dead_cell_with_three_neighbors_is_born() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(1, 1, Cell::Dead);
        game.set_cell_state(0, 0, Cell::Alive);
        game.set_cell_state(2, 2, Cell::Alive);
        game.set_cell_state(2, 0, Cell::Alive);
        game.determine_next_state();
        assert_eq!(game.get_cell_state(1, 1), Cell::Alive);
    }

    #[test]
    fn no_living_neighbors() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(1, 1, Cell::Alive);
        let living_neighbors = game.get_number_of_living_neighbors(1, 1);
        assert_eq!(living_neighbors, 0);
    }

    #[test]
    fn one_living_neighbor() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(0, 0, Cell::Alive);
        let living_neighbors = game.get_number_of_living_neighbors(1, 1);
        assert_eq!(living_neighbors, 1);
    }

    #[test]
    fn two_living_neighbors() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(0, 0, Cell::Alive);
        game.set_cell_state(2, 2, Cell::Alive);
        let living_neighbors = game.get_number_of_living_neighbors(1, 1);
        assert_eq!(living_neighbors, 2);
    }

    #[test]
    fn two_living_neighbors_corner_cell() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(0, 1, Cell::Alive);
        game.set_cell_state(1, 0, Cell::Alive);
        let living_neighbors = game.get_number_of_living_neighbors(0, 0);
        assert_eq!(living_neighbors, 2);
    }

    #[test]
    fn three_living_neighbors_corner_cell() {
        let mut game = Game::new(3, 3);
        game.set_cell_state(0, 1, Cell::Alive);
        game.set_cell_state(1, 0, Cell::Alive);
        game.set_cell_state(1, 1, Cell::Alive);
        let living_neighbors = game.get_number_of_living_neighbors(0, 0);
        assert_eq!(living_neighbors, 3);
    }
}
